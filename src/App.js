import "./App.css";
import { lightTheme } from "./utils/Themes";
import styled, { ThemeProvider } from "styled-components";
import Navbar from "./components/Navbar";
import Hero from "./components/HeroSection/index";
import Skills from "./components/Skills/index";
import Education from "./components/Education/index";
import Experience from "./components/Experience";
import Projects from "./components/Projects/index";
import Footer from "./components/Footer";
import Contact from "./components/Contact/index";
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";

const Body = styled.div`
  background-color: ${({ theme }) => theme.bg};
  width: 100%;
  height: 100%;
  overflow-x: hidden;
`;

const Wrapper = styled.div`
  background: linear-gradient(
      38.73deg,
      rgba(204, 0, 187, 0.15) 0%,
      rgba(201, 32, 184, 0) 50%
    ),
    linear-gradient(
      141.27deg,
      rgba(0, 70, 209, 0) 50%,
      rgba(0, 70, 209, 0.15) 100%
    );
  width: 100%;
  clip-path: polygon(0 0, 100% 0, 100% 100%, 30% 98%, 0 100%);
`;

function App() {
  return (
    <ThemeProvider theme={lightTheme}>
      <Router>
        <Navbar />
        <Body>
          <Hero />
          <Wrapper>
            <Skills />
            <Experience />
          </Wrapper>
          <Projects />
          <Wrapper>
            <Education />
            <Contact />
          </Wrapper>
          <Footer />
          <Routes>
            <Route path="/github-profile" element={<Navigate to="https://github.com/Arjunan1234" />} />
          </Routes>
        </Body>
      </Router>
    </ThemeProvider>
  );
}

export default App;
