export const Bio = {
  name: "Arjunan",
  roles: ["React Developer", "Programmer"],
  description:
    "I am a motivated and versatile individual, always eager to take on new challenges. With a passion for learning I am dedicated to delivering high-quality results. With a positive attitude and a growth mindset, I am ready to make a meaningful contribution and achieve great things.",
  github: "https://github.com/Arjunan1234",
  resume:
    "https://drive.google.com/file/d/1ffZrcMcn8UatXGIaautbbqpV7ADNaETA/view?usp=sharing",
  linkedin: "https://www.linkedin.com/in/arjunans2108",
  insta: "https://www.instagram.com/arjun_2821_/",
  facebook: "https://www.facebook.com/rishav.chanda.165/",
};

export const skills = [
  {
    title: "Frontend",
    skills: [
      {
        name: "React Js",
        image:
          "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0xMS41IC0xMC4yMzE3NCAyMyAyMC40NjM0OCI+CiAgPHRpdGxlPlJlYWN0IExvZ288L3RpdGxlPgogIDxjaXJjbGUgY3g9IjAiIGN5PSIwIiByPSIyLjA1IiBmaWxsPSIjNjFkYWZiIi8+CiAgPGcgc3Ryb2tlPSIjNjFkYWZiIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIi8+CiAgICA8ZWxsaXBzZSByeD0iMTEiIHJ5PSI0LjIiIHRyYW5zZm9ybT0icm90YXRlKDYwKSIvPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIiB0cmFuc2Zvcm09InJvdGF0ZSgxMjApIi8+CiAgPC9nPgo8L3N2Zz4K",
      },

      {
        name: "HTML",
        image: "https://www.w3.org/html/logo/badge/html5-badge-h-solo.png",
      },

      {
        name: "CSS",
        image:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/1452px-CSS3_logo_and_wordmark.svg.png",
      },
      {
        name: "JavaScript",
        image:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/JavaScript-logo.png/800px-JavaScript-logo.png",
      },
      {
        name: "Typescript",
        image:
          "https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg",
      },
      {
        name: "Bootstrap",
        image:
          "https://getbootstrap.com/docs/5.3/assets/brand/bootstrap-logo-shadow.png",
      },
    ],
  },
  {
    title: "Backend",
    skills: [
      {
        name: "Java",
        image:
          "https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg",
      },

      {
        name: "Python",
        image:
          "https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg",
      },

      {
        name: "MongoDB",
        image:
          "https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg",
      },
    ],
  },
  {
    title: "Devops",
    skills: [
      {
        name: "AWS",
        image:
          "https://www.logo.wine/a/logo/Amazon_Web_Services/Amazon_Web_Services-Logo.wine.svg",
      },
      {
        name: "Docker",
        image:
          "https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg",
      },
    ],
  },
  {
    title: "Others",
    skills: [
      {
        name: "Git",
        image: "https://git-scm.com/images/logos/logomark-orange@2x.png",
      },
      {
        name: "GitHub",
        image:
          "https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png",
      },

      {
        name: "GitLab",
        image:
          "https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/144_Gitlab_logo_logos-512.png",
      },
      {
        name: "Netlify",
        image:
          "https://seeklogo.com/images/N/netlify-logo-BD8F8A77E2-seeklogo.com.png",
      },
      {
        name: "VS Code",
        image:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/512px-Visual_Studio_Code_1.35_icon.svg.png?20210804221519",
      },
      {
        name: "Postman",
        image:
          "https://uxwing.com/wp-content/themes/uxwing/download/brands-and-social-media/postman-icon.png",
      },
    ],
  },
];

export const experiences = [
  {
    id: 0,
    img: "",
    role: "Product Engineer Intern",
    company: "Sketch Brahma Technologies Pvt Ltd",
    date: "Mar 2024 - Present",
    desc: "Developed responsive user interfaces using React.js, HTML, CSS, and JavaScript.Integrated RESTful APIs for seamless data communication between frontend and backend.Enhanced code quality and maintainability by utilizing TypeScript for type safety and improved developer experience.",
    skills: ["HTML", "CSS", "BOOTSTRAP", "JAVASCRIPT", "REACT JS"],
  },
];

export const education = [
  {
    id: 0,
    school: "K L N College of Engineering , Madurai",
    date: "Oct 2022 - Jun 2024",
    grade: "8.1 CGPA",
    desc: "I am currently pursuing a Master's degree in Computer Science and Engineering at KLN College of Engineering , Madurai I have completed 3 semesters and have a CGPA of 8.1. ",
    degree: "Bachelor of Technology - BTech, Computer Science and Engineering",
  },
  {
    id: 1,
    school: "Sri Paramakalyani College , Alwarkurchi",
    date: "Apr 2019 - Apr 2022",
    grade: "85%",
    desc: "I completed my Bsc Computer Science  at sri pramakalyani college , Tirunelveli, where I studied Science with Computer Science.",
    degree: "Bsc Computer Science",
  },
  {
    id: 2,
    school: "st Johns Hr Sec  School, Tirunelveli",
    date: "Apr 2017 - Apr 2019",
    grade: "88%",
    desc: "I completed my class 12 education at st Johns School, Tirunelveli, where I studied Science with Computer Application.",
    degree: "HSC Computer Science",
  },
];

export const projects = [
  {
    id: 0,
    title: "Movie DB Website",
    date: "Mar 2024",
    description:
      "Developed a comprehensive Movie Database website utilizing React.js as the frontend framework. The application was designed to be dynamic and interactive, leveraging HTML, CSS, and JavaScript for the user interface.",
    image:
      "https://gitlab.com/-/project/56617011/uploads/86a28a9a570e6bfb9fed3aa976d9c924/Screenshot_from_2024-06-17_11-50-08.png",
    tags: ["React js", "Javascript", "Html", "Css"],
    gitHub: "https://gitlab.com/arjunan2/moviedb-task",
    live: "https://arjunmoviedb.netlify.app/",
  },
  {
    id: 1,
    title: "Portfolio",
    date: "Jan 2024",
    description:
      "Developed a personal portfolio website using React.js for the frontend framework, along with HTML, CSS, and JavaScript to create a dynamic and interactive user interface. Leveraged styled-components for enhanced styling and component-based design. This project highlights my expertise in frontend development and my ability to design aesthetically pleasing and responsive web applications.",
    image:
      "https://gitlab.com/-/project/56617011/uploads/86a28a9a570e6bfb9fed3aa976d9c924/Screenshot_from_2024-06-17_11-50-08.png",
    tags: ["React js", "Javascript", "Html", "Css", "StyledComponent"],
    gitHub: "https://gitlab.com/learning2333410/static-websites/portfolio",
    live: "https://arjunan.netlify.app/",
  },

  {
    id: 2,
    title: "E-Commerce",
    date: "Jan 2024",
    description:
      "Developed a comprehensive e-commerce web app using Django, HTML, CSS, Bootstrap, JavaScript, and SQLite. Admin features encompassed category and product management, user account updates, and order processing. Non-registered users could browse products, while registered users could add to the cart and track order status.",
    image:
      "https://private-user-images.githubusercontent.com/63443618/301886128-c84931ee-de81-4faf-a29f-7e3e3add3c0e.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTg2MDQ2ODQsIm5iZiI6MTcxODYwNDM4NCwicGF0aCI6Ii82MzQ0MzYxOC8zMDE4ODYxMjgtYzg0OTMxZWUtZGU4MS00ZmFmLWEyOWYtN2UzZTNhZGQzYzBlLnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA2MTclMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNjE3VDA2MDYyNFomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPTIwOWU5NDFhZjY5ZDc1NDNiZDliMzJkMzMzZDUyZWZjNzY3YTZlYTU3ZjQwNmFkMzUxNTc1M2IzMmM5MzcxYWMmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.wfGAezYjhPavErTIraBd19D0-QdacVD5sZ10ExZbi1I",
    tags: ["Python", "Django", "SqlLite", "Html", "Css", "Javascript"],
    gitHub: "https://github.com/rishavchanda/Face-Recodnition-AI-with-Python",
    webapp: "https://github.com/rishavchanda/Face-Recodnition-AI-with-Python",
  },
  {
    id: 3,
    title: "Video Call",
    date: "Dec 2024",
    description:
      "Developed a video calling application using Django for backend logic, and HTML, CSS, and JavaScript for the frontend. Integrated video call APIsto enable real-time communication. Designed an intuitive and responsive user interface for seamless user experience. This project showcases my proficiency in full-stack development and my ability to implement complex features for modern web applications.",
    image:
      "https://private-user-images.githubusercontent.com/63443618/297966592-a2bacbe7-40da-43ab-98f1-f36399cf3d64.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTg2MDUxMTYsIm5iZiI6MTcxODYwNDgxNiwicGF0aCI6Ii82MzQ0MzYxOC8yOTc5NjY1OTItYTJiYWNiZTctNDBkYS00M2FiLTk4ZjEtZjM2Mzk5Y2YzZDY0LnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA2MTclMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNjE3VDA2MTMzNlomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPTE4ZGIyMjc5MWZlZmU3OTI3MjZhMTQ2ZmE5MDEyYjVmMThlN2MwM2UxNmQ3OWFlZWM1MWVlNzc4NDgwZTQ2ZDcmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.maWc8R3twdgvdxBqpOaXshzSddd4aPwFRvIxFssA6e8",
    tags: ["Python", "Django", "SqlLite", "Html", "Css", "Javascript"],
    gitHub: "https://github.com/Arjunan1234/videocall-django",
    webapp: "https://github.com/rishavchanda/Face-Recodnition-AI-with-Python",
  },
];
